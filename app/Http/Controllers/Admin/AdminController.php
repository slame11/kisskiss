<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 12.03.2019
 * Time: 09:05
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (session()->has('locale')) {
                App::setLocale(session('locale'));
            }
            if (!session()->has('currency')) {
                session(['currency' => env('DEFAULT_CURRENCY')]);
                session(['rate' => 1]);
            }
            return $next($request);
        });
    }

    public function setLang($locale)
    {
        App::setLocale($locale);
        session(['locale'=>$locale]);

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.dashboard.dashboard',
            [

            ]
        );
    }

}
