<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Requests\CategoryValidation;
use App\Models\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends AdminController
{
    public function __construct()
    {
        $this->middleware('hasPermission:admin');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::whereNotNull('id')->with('parent')->paginate($this->paginate);
        return view('admin.category.index', [
            'categories'=>$categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->pluck('title','id');
        return view('admin.category.create',[
            'category'=>$category,
            'categories_global'=>$categories_global,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryValidation $request)
    {
        $category = Category::create($request->all());
        $category->save();
        return redirect()
            ->route('category.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->pluck('title','id');
        return view('admin.category.edit',[
            'category' => $category,
            'categories_global'=>$categories_global,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryValidation $request, Category $category)
    {
        $category->update($request->all());
        return redirect()
            ->route('category.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()
            ->route('category.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

    public function packet(Request $request)
    {
        if ($request->all())
        {
            $data = $request->all();
            $categories = [];
            if( !empty($data['packet']) )
            {
                $rows = explode("\n",$data['packet']);
                foreach ($rows as $row)
                {
                    if(strlen($row)>0) {
                        $category = explode($data['explode_symbol'], $row);
                        array_push($categories, [
                            'title' => $category[0],
                            'meta' => $category[1] ?? null,
                            'description' => $category[2] ?? null,
                            'text' => $category[3] ?? null,
                            'parent_id' => $data['parent_id'],
                        ]);
                    }
                }
            }
            if ($request->hasFile('category_packet')) {
                $file_content = File::get($request->file('category_packet')->getPathname());
                $rows = explode("\n",$file_content);
                foreach ($rows as $row)
                {
                    if(strlen($row)>0) {
                        $category = explode($data['explode_symbol'], $row);
                        array_push($categories, [
                            'title' => $category[0],
                            'meta' => $category[1] ?? null,
                            'description' => $category[2] ?? null,
                            'text' => $category[3] ?? null,
                            'parent_id' => $data['parent_id'],
                        ]);
                    }
                }
            }

            $validator_fails = '';

            foreach ($categories as $category)
            {
                $validator = Validator::make($category, [
                    'title' => [
                        'required',
                        'string',
                        'max:255',
                        Rule::unique('categories')->where(function ($query) use($category,$data) {
                            return $query->where('title', $category['title'])
                                ->where('parent_id', $data['parent_id']);
                        }),
                    ],
                    'meta' => 'nullable',
                    'description' => 'nullable',
                    'text' => 'nullable',
                    'parent_id' => 'required|integer|exists:categories,id',
                ]);
                if ($validator->fails()) {
                    foreach ($validator->errors()->messages() as $key=>$val)
                    {
                        $validator_fails .= "$category[$key] - $val[0] \r\n";
                    }
                }
                else
                {
                    $c = Category::create($category);
                    $c->save();
                }
            }
//            dd($validator_fails);

            return redirect()
                ->route('category.index')
                ->with( 'message.level', 'success' )
                ->with( 'message.content', __('general.creation_success') )
                ->with( 'message.level', 'error' )
                ->with( 'message.content', $validator_fails );
        }

        $category = new Category();
        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->pluck('title','id');
        return view('admin.category.packet',[
            'category' => $category,
            'categories_global'=>$categories_global,
        ]);
    }

    public function multidelete(Request $request)
    {
//        dd($request->all());
        $categories = Category::findMany(json_decode($request->get('ids'),true));
        $categories->each->delete();
        return redirect()
            ->route('category.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
}
