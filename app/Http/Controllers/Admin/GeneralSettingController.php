<?php

namespace App\Http\Controllers\Admin;

use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImageTool;


/**
 * Are you idiot or what? Why did you extend AdminControlers from Controller?
 * Really?
 * You can look to another controllers and you can see whole
 * controler in admin groups extends from AdminController
 */
class GeneralSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $settings = GeneralSetting::whereIn('key', [
           'general_address',
           'general_phone_1',
           'general_phone_2',
           'general_email',
           'general_site_url'
       ])->get();

        return view('admin.settings.index',
        [
            'settings' => $settings,
            'top_logo' =>  GeneralSetting::where('key','top_logo')->first()['value'],
            'top_logo_thumb' => ImageTool::resize(GeneralSetting::where('key','top_logo')->first()['value'], 0, 0),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('setting')){
            foreach ($request->input('setting') as $key => $value) {
                GeneralSetting::where('key', $key)->update(array('value' => $value));
            }
        }

        return redirect()->route('generalSettings.index')->with('general-settings-success', __('general.settings_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
