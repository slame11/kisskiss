<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ImageCategory;
use App\Models\ImageDB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class AppController extends Controller
{
    //
    public function index(Request $request)
    {
        $data = $request->all();

        $type = ['gif','pic'];
        $sort = 'id+asc';
        if(isset($data['type']) && !empty($data['type']) && (strtolower($data['type']) != 'type') )
        {
            $type = [];
            $type[] = $data['type'];
        }
        if(isset($data['sort']) && !empty($data['sort']) && (strtolower($data['sort']) != 'sort') )
        {
            $sort = $data['sort'];
        }
        $orderBy = explode('+',$sort);
        $images = ImageDB::whereNotNull('id')
                            ->whereIn('type',$type)
                            ->orderBy($orderBy[0],$orderBy[1])
                            ->paginate(18);

        if(!isset($data['type']) && empty($data['type'])){
            $data["type"]="Type";
        }
        if(!isset($data['sort']) && empty($data['sort'])){
            $data["sort"]="Sort";
        }

        return view('app.index', [
            'images'=>$images,
            'i'=>0,
            'data'=>$data,
        ]);
    }

    public function image($id,Request $request)
    {
        $image = ImageDB::find($id);
        return view('app.image', [
            'image'=>$image,
        ]);
    }

    public function download(Request $request){
        $all = $request->all();
        if(isset($all['image_id'])){
            $image = ImageDB::where('id',(int)$all['image_id'])->firstOrFail()->src();
            $basename = $image;
            $filename =  $basename; // don't accept other directories

            $mime = ($mime = getimagesize($filename)) ? $mime['mime'] : $mime;
            $size = filesize($filename);
            $fp   = fopen($filename, "rb");


            header("Content-type: " . $mime);
            header("Content-Length: " . $size);
            // NOTE: Possible header injection via $basename
            header("Content-Disposition: attachment; filename=" . $basename);
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            return fpassthru($fp);
        }

    }

    public function category($category,$subcategory = null,$subsubcategory = null, Request $request)
    {
        $data = $request->all();
//        dd($data);
        $type = ['gif','pic'];
        $sort = 'id+asc';
        if(isset($data['type']) && !empty($data['type']) && (strtolower($data['type']) != 'type') )
        {
            $type = [];
            $type[] = $data['type'];
        }
        if(isset($data['sort']) && !empty($data['sort']) && (strtolower($data['sort']) != 'sort') )
        {
            $sort = $data['sort'];
        }
        $orderBy = explode('+',$sort);
        $url = $category;
        $subcategory ? $url .= '/'.$subcategory : null ;
        $subsubcategory ? $url .= '/'.$subsubcategory : null ;

        $categoryModel = Category::getByUrl_noslug($url);
        $imgIds = ImageCategory::where('category_id',$categoryModel->id)
                                ->pluck('image_id');
        $images = ImageDB::whereIn('id',$imgIds)
                        ->whereIn('type',$type)
                        ->orderBy($orderBy[0],$orderBy[1])
                        ->paginate(18);

        if(!isset($data['type']) && empty($data['type'])){
            $data["type"]="Type";
        }
        if(!isset($data['sort']) && empty($data['sort'])){
            $data["sort"]="Sort";
        }

        return view('app.index', [
            'images'=>$images,
            'category'=>$categoryModel,
            'i'=>0,
            'data'=>$data,
        ]);
    }
}
