<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    public function api($class,$method, Request $request)
    {
        $class = ucwords($class);
        $class = 'App\\Models\\Api\\'.$class;
        if(class_exists($class) && method_exists($class,$method)) {
            return $class::$method($request);
        }
        return response()->json([]);
    }
}
