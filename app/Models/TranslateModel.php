<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TranslateModel extends Model
{
    /**
     * @param string $status
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getLocale($status='create', $field='name')
    {
        $locales = config('translatable.locales');
        if($status == 'create') {
            foreach(config('translatable.locales') as $locale) {
                $this->translateOrNew($locale)->$field = '';
            }
        }
        $translations = $this->translations->toArray();
        foreach ($locales as $key => $lang) {
            $item = array_search($lang, array_column($translations, 'locale'));
            if(false !== $item) {
                $locales[$key] = $translations[$item];
            }else{
                $locales[$key] = ['locale' => $lang, $field => '',];
            }
        }
        return $locales;
    }

    public function availableTranslation($lang)
    {
        if (isset($this->attributes['available_translation'])) {
            $data = explode(',', $this->attributes['available_translation']);
            if (array_search($lang, $data) !== false) {
                return true;
            }
        }
        return false;
    }

    public function setAvailableTranslationAttribute($request)
    {
        $data = [];
        foreach (config('translatable.locales') as $locale) {
            if (isset($request['available_translation-' . $locale]) && $request['available_translation-' . $locale] == 1) {
                array_push($data, $locale);
            }
        }
        if(! empty($data)) {
            $this->attributes['available_translation'] = implode(',', $data);
        }else{
            $this->attributes['available_translation'] = null;
        }
    }
}
