<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageDB extends Model
{
    public $fillable = ['title','type','path','thumbnail','created_by','modified_by',];

    public function src()
    {
        return str_replace('public','storage',$this->path);
    }
    public function thumbnail()
    {
        return str_replace('public','storage',$this->thumbnail);
    }

    public function categories()
    {
        return $this->hasManyThrough('App\Models\Category','App\Models\ImageCategory','image_id','id','id','category_id');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User','id','created_by');
    }

    public function getCategories()
    {
        return $this->categories()->getRelated();
    }

    public function getAuthor()
    {
        return $this->author()->getRelated();
    }

    public function getWidth($height = null)
    {
        $size = getimagesize(storage_path('app/'.$this->path));
        $width = $size[0];
        $c_height = $size[1];
        if($height)
        {

            $width = $width/($c_height/100);
        }
        return $width;
    }

    public function getHeight($width = null)
    {
        $size = getimagesize(storage_path('app/'.$this->path));
        $c_width = $size[0];
        $height = $size[1];
        if($width)
        {
            $height = $height/($c_width/100);

        }
        return $height;
    }
}
