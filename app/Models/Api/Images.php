<?php

namespace App\Models\Api;

use App\Models\ImageDB;

class Images
{
    //

    public static function one($request)
    {
        $all = $request->all();
        $image = null;
        if(isset($all['id'])) {
            $image = ImageDB::where('id',$all['id'])
                            ->select(['id','title','created_at','updated_at','type','thumbnail','path','created_by'])
                            ->with('author','categories')
                            ->first()
            ;
            $image->src = $image->src();
            $image->thumbnail = $image->thumbnail();
            unset($image->path);
            unset($image->created_by);
        }
        return response()->json($image);
    }

    public static function all($request)
    {
        $all = $request->all();
        $images = null;
        $per_page = 15;
        if(isset($all['per_page']) && !empty($all['per_page']) )
        {
            $per_page = $all['per_page'];
        }
        else {
            $all['per_page'] = $per_page;
        }
        if(isset($all['id'])) {
            $images = ImageDB::whereIn('id', $all['id'])
                            ->select(['id','title','created_at','updated_at','type','thumbnail','path','created_by'])
                            ->with('author','categories')
                            ->paginate($per_page)
            ;
        }
        else {
            $images = ImageDB::select(['id','title','created_at','updated_at','type','thumbnail','path','created_by'])
                            ->with('author','categories')
                            ->paginate($per_page)
            ;
        }
        if(!empty($images)){
            foreach ($images as $image) {
                $image->src = $image->src();
                $image->thumbnail = $image->thumbnail();
                unset($image->path);
                unset($image->created_by);
            }
        }
        return response()->json($images);
    }

    public static function download($request){
        $all = $request->all();
        if(isset($all['id'])){
            $image = ImageDB::where('id',(int)$all['id'])->firstOrFail()->src();
            $basename = $image;
            $filename =  $basename; // don't accept other directories

            $mime = ($mime = getimagesize($filename)) ? $mime['mime'] : $mime;
            $size = filesize($filename);
            $fp   = fopen($filename, "rb");


            header("Content-type: " . $mime);
            header("Content-Length: " . $size);
            // NOTE: Possible header injection via $basename
            header("Content-Disposition: attachment; filename=" . $basename);
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            return fpassthru($fp);
        }
        return null;
    }
}
