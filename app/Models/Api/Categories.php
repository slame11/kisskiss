<?php

namespace App\Models\Api;

use App\Models\Category;
use App\Models\ImageCategory;
use App\Models\ImageDB;

class Categories
{
    //
    public static function all($request)
    {
        $data = $request->all();
        $type = ['gif','pic'];
        $sort = 'id+asc';
        $per_page = 15;
        $img_per_cat = 15;
        if(isset($data['type']) && !empty($data['type']) && (strtolower($data['type']) != 'type') )
        {
            $type = [];
            $type[] = $data['type'];
        }
        else {
            $data['type'] = $type;
        }
        if(isset($data['sort']) && !empty($data['sort']) && (strtolower($data['sort']) != 'sort') )
        {
            $sort = $data['sort'];
        }
        else {
            $data['sort'] = $sort;
        }
        if(isset($data['per_page']) && !empty($data['per_page']) )
        {
            $per_page = $data['per_page'];
        }
        else {
            $data['per_page'] = $per_page;
        }
        if(isset($data['img_per_cat']) && !empty($data['img_per_cat']) )
        {
            $img_per_cat = $data['img_per_cat'];
        }
        else {
            $data['img_per_cat'] = $img_per_cat;
        }
        $orderBy = explode('+',$sort);
        $categoryModel = Category::paginate($per_page);
        $images = [];
        $imagesUniqIds = [];
        if(isset($data['id']) && !empty($data['id']) )
        {
            $categoryModel = Category::whereIn('id',$data['id'])->paginate($per_page);
        }
        foreach ($categoryModel as $category) {
            $imgIds = ImageCategory::where('category_id',$category->id)
                ->pluck('image_id');
            $category->images = ImageDB::whereIn('id',$imgIds)
                ->whereIn('type',$type)
                ->orderBy($orderBy[0],$orderBy[1])
                ->with('author','categories')
                ->paginate($img_per_cat,['*'],'img_page');

            if(!empty($category->images)){
                foreach ($category->images as $image) {
                    $image->src = $image->src();
                    $image->thumbnail = $image->thumbnail();
                    unset($image->path);
                    unset($image->created_by);
                    if(!in_array($image->id,$imagesUniqIds)) {
                        array_push($images,$image);
                        array_push($imagesUniqIds,$image->id);
                    }
                }
            }
        }
        dd($categoryModel);

        return response()->json([
            'categories' => $categoryModel,
            '$images' => $images,
            'data' => $data,
        ]);
    }

    public static function one($request)
    {
        $data = $request->all();
        $type = ['gif','pic'];
        $sort = 'id+asc';
        $img_per_cat = 15;
        if(isset($data['type']) && !empty($data['type']) && (strtolower($data['type']) != 'type') )
        {
            $type = [];
            $type[] = $data['type'];
        }
        else {
            $data['type'] = $type;
        }
        if(isset($data['sort']) && !empty($data['sort']) && (strtolower($data['sort']) != 'sort') )
        {
            $sort = $data['sort'];
        }
        else {
            $data['sort'] = $sort;
        }
        if(isset($data['img_per_cat']) && !empty($data['img_per_cat']) )
        {
            $img_per_cat = $data['img_per_cat'];
        }
        else {
            $data['img_per_cat'] = $img_per_cat;
        }
        $orderBy = explode('+',$sort);
        $id = $data['id'];

        $categoryModel = Category::find($id);
        $imgIds = ImageCategory::where('category_id',$id)
            ->pluck('image_id');
        $images = ImageDB::whereIn('id',$imgIds)
            ->whereIn('type',$type)
            ->orderBy($orderBy[0],$orderBy[1])
            ->with('author','categories')
            ->paginate($img_per_cat,['*'],'img_page');

        if(!empty($images)){
            foreach ($images as $image) {
                $image->src = $image->src();
                $image->thumbnail = $image->thumbnail();
                unset($image->path);
                unset($image->created_by);
            }
        }

        return response()->json([
            'category'=>$categoryModel,
            'images'=>$images,
            'data'=>$data,
        ]);
    }
}
