<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/instaporn', 'AppController@index')->name('instaporn');
Route::get('/instaporn/download', 'AppController@download')->name('instaporn_download');
//Route::get('/instaporn/{category}', 'AppController@category');
//Route::get('/instaporn/{category}/{subcategory}', 'AppController@category');
Route::get('/instaporn/{category}/{subcategory?}/{subsubcategory?}', 'AppController@category')->name('instaporn-category');
Route::get('/image/{id}', 'AppController@image')->name('image');
//Route::get('/category/{id}', 'AppController@category')->name('image-category');

Auth::routes(['verify' => true]);

// admin-lte
Route::prefix( '/admin' )->group( function () {

    Route::get('/admin-lte/{locale}', 'Admin\AdminController@setLang')->name('lang');
    Route::get('/', 'Admin\AdminController@index')->name('dashboard');

    Route::resource('/users','Admin\UserController');
    Route::resource('/countries','Admin\CountryController');
    Route::resource('/cities','Admin\CityController');

    // file manager
    Route::get('/filemanager','Admin\FilemanagerController@index')->name('filemanager');
    Route::get('/filemanager/show','Admin\FilemanagerController@show')->name('show-filemanager');
    Route::post('/filemanager/upload','Admin\FilemanagerController@upload')->name('upload-items');
    Route::post('/filemanager/new','Admin\FilemanagerController@folder')->name('new-folder');
    Route::post('/filemanager/delete','Admin\FilemanagerController@delete')->name('delete-items');

    //Settings
    Route::resource('/generalSettings','Admin\GeneralSettingController');

    //Categories for images
    Route::post('/category/multidelete','Admin\CategoryController@multidelete')->name('category.multi-delete');
    Route::get('/category/packet','Admin\CategoryController@packet')->name('category.new-packet');
    Route::post('/category/packet','Admin\CategoryController@packet')->name('category.create-packet');
    Route::resource('/category','Admin\CategoryController');

    Route::any('/images/category/{id}','Admin\ImageController@category')->name('admin.image.by-category');
    Route::any('/images/delete','Admin\ImageController@delete')->name('images.delete');
    Route::resource('/images','Admin\ImageController');

    Route::get('/google-search/search','Admin\GSearchController@search')->name('google-search.search');
    Route::post('/google-search/search','Admin\GSearchController@search')->name('google-search.search');
    Route::post('/google-search','Admin\GSearchController@store')->name('google-search.store');

    Route::resource('/google-search','Admin\GSearchController');

});
