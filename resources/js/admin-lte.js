try {
    window.$ = window.jQuery = require('jquery');
    require('admin-lte');
    require('bootstrap-sass');
    require('bootstrap-3-typeahead');
    require('select2');
    require('isotope-layout/dist/isotope.pkgd');
    require('@fancyapps/fancybox');
    require('./jquery.form');
} catch (e) {}

$(function () {
    let Chart = require('chart.js/dist/Chart.js');

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});


    var grid = $('.grid').isotope({
        // options
        itemSelector: '.grid-item'
    });

    grid.on( 'click', 'button.remove_item', function() {
        // remove clicked element
        grid.isotope( 'remove', this.closest('.grid-item') )
        // layout remaining item elements
            .isotope('layout');
    });
    // test comment


    // Image Manager
    $(document).on('click', 'a[data-toggle=\'image\']', function(e) {
        var $element = $(this);
        var $popover = $element.data('bs.popover'); // element has bs popover?

        e.preventDefault();

        // destroy all image popovers
        $('a[data-toggle="image"]').popover('destroy');

        // remove flickering (do not re-add popover when clicking for removal)
        if ($popover) {
            return;
        }

        $element.popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function() {
                return '<a type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></a> <a type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
            }
        });

        $element.popover('show');

        $('#button-image').on('click', function() {
            var $button = $(this);
            var $icon   = $button.find('> i');

            $('#modal-image').remove();

            $.ajax({
                url: '/admin/filemanager/show?target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
                dataType: 'html',
                beforeSend: function() {
                    $button.prop('disabled', true);
                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-circle-o-notch fa-spin');
                    }
                },
                complete: function() {
                    $button.prop('disabled', false);

                    if ($icon.length) {
                        $icon.attr('class', 'fa fa-pencil');
                    }
                },
                success: function(html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                    $('#modal-image').modal('show');
                }
            });

            $element.popover('destroy');
        });

        $('#button-clear').on('click', function() {
            $element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

            $element.parent().find('input').val('');

            $element.popover('destroy');
        });
    });


    //upload imagees
    if($('form.image_upload_form').length > 0){
        var bar = $('.bar');
        var percent = $('.percent');
        var images_list = $('.images_list');
        var images = [];

        $('form.image_upload_form').ajaxForm({
            beforeSubmit: function(formData, jqForm, options) {
                images = formData.filter(data => data.type === 'file');
                if(!images[0].value){
                    return false;
                }
                $('.progress').show();
                var sizeSum = 0;
                for(var i = 0; i < images.length; i++){
                    sizeSum += images[i].value.size;
                    images[i].sumPosition = sizeSum;
                    images_list.append('<li>' + images[i].value.name + '</li>');
                }
                images = images.reverse();
            },
            beforeSend: function() {
                var percentVal = '';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
                var uploadedIndex = images.length - images.findIndex(img => img.sumPosition <= position);
                if(uploadedIndex){
                    images_list.children().slice(0,uploadedIndex + 1).attr("class","uploaded");
                }
            },
            success: function() {
                var percentVal = 'Saving';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                window.location.href = "/admin/images";
            }
        });
    }
});
