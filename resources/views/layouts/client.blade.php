<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>Document</title>
</head>
<body>

<section class="categories-list invisible" id="navbarSupportedContent">

    <a href="#" class="close_sidebar">+</a>

    <a class="button_in_sidebar d-block d-sm-none" href="https://kisskiss.show/livecam/authorization">Login</a>

    <input class="search_in_sidebar d-block d-lg-none" placeholder="Search"/>

    <p class="section_title d-block d-lg-none">Menu</p>
    <ul class="category_wrapper d-block d-lg-none">
        <li class="category_wrapper_item">
            <span class="li_wrapper has_children">
                <a href="https://kisskiss.show/livecam" class="has_children_a">
                    <span class="parent_name">Livecams</span>
                </a>
                <span class="super_span"><span class="arrow-down"></span></span>
            </span>

            <ul class="inner_ul hidden_default">
                <li><a href="https://kisskiss.show/livecam/filter/anal">Anal</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/blondes">Blondes</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/blowjob">Blowjob</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/brunettes">Brunettes</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/couples">Couples</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/fingering">Fingering</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/lesbian">Lesbian</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/redhead">Redhead</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/sex_toys">Sex toys</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/small_tits">Small Tits</a></li>
                <li><a href="https://kisskiss.show/livecam/filter/threesome">Threesome</a></li>
            </ul>
        </li>
        <li class="category_wrapper_item">
            <a href="https://kisskiss.show/dating">
                <span class="parent_name">Sexdating</span>
            </a>
        </li>
        <li class="category_wrapper_item">
            <span class="li_wrapper has_children">
                <a href="https://kisskiss.show/videos">
                    <span class="parent_name">Videos</span>
                </a>
                <span class="super_span"><span class="arrow-down"></span></span>
            </span>

            <ul class="inner_ul hidden_default">
                <li><a href="https://kisskiss.show/videos?filters=amateur">Amateur</a></li>
                <li><a href="https://kisskiss.show/videos?filters=big_tits">Big tits</a></li>
                <li><a href="https://kisskiss.show/videos?filters=fingering">Fingering</a></li>
                <li><a href="https://kisskiss.show/videos?filters=webcam">Webcam</a></li>
            </ul>
        </li>
        <li class="category_wrapper_item">
            <a href="https://kisskiss.show/articles">
                <span class="parent_name">Articles</span>
            </a>
        </li>
        <li class="category_wrapper_item">
            <span class="li_wrapper has_children">
                <a href="https://kisskiss.show/instaporn">
                    <span class="parent_name">Instaporn</span>
                </a>
                <span class="super_span"><span class="arrow-down"></span></span>
            </span>

            <ul class="inner_ul hidden_default">
                @foreach(\App\Models\Category::withoutParents() as $category)
                    <li><a href="{{route('instaporn-category',$category->url_noslug())}}">{{$category->title}}</a></li>
                @endforeach
            </ul>
        </li>
    </ul>

    <p class="section_title d-block d-lg-none">Instaporn</p>
    <ul class="category_wrapper">
        @foreach(\App\Models\Category::withoutParents() as $category)
            <li class="category_wrapper_item">
                <span class="li_wrapper {{ !empty($category->children) && count($category->children)>0 ? 'has_children' : '' }}">
                    <a href="{{route('instaporn-category',$category->url_noslug())}}" class="{{ !empty($category->children) && count($category->children)>0 ? 'has_children_a' : '' }}">
                        <span class="parent_name">{{$category->title}}</span>
                    </a>
                    @if(!empty($category->children) && count($category->children)>0)
                        <span class="super_span"><span class="arrow-down"></span></span>
                    @endif
                </span>

                @if(!empty($category->children) && count($category->children)>0)
                    <ul class="inner_ul hidden_default">
                        @foreach($category->children as $child)
                            <li>
                                <a href="{{route('instaporn-category',$child->url_noslug())}}"> {{$child->title}} </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
</section>
<div class="overlay hiden-overlay"></div>

<header>
    <div class="container-fluid px-0">
        {{--<nav class="navbar navbar-dark navbar-expand-lg navbar-custom px-2">--}}
            <div class="header-up-wrapper">
                <div class="inner-wrapper">
                    <div class="logo-and-button">
                        <a class="logo" href="/">
                            <img src="{{asset('img/logo.png')}}">
                        </a>
                    </div>

                    <div class="categories-header d-none d-lg-flex">
                        <form class="form-inline my-2 my-lg-0 custom-search">
                            <input class="form-control mr-sm-2 h_search_input" placeholder="Search" aria-label="Search">
                            <input class="h_input h_search_submit" type="submit" value="">
                        </form>

                    </div>

                    <div class="login-data-header d-none d-sm-flex">
                    @guest
                        <a class="h_username login" href="{{route('login')}}">{{ __('Login') }}</a>
                        @if (Route::has('register'))
                            <a class="h_username login" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else
                        <div class="h_profile d-block">
                            <a class="h_username" href="https://kisskiss.show/livecam/clientworkpage">{{ Auth::user()->name }}</a>
                            <a class="h_credit">200 cred</a>
                        </div>
                        <a class="exit d-block" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            <img height="31" src="{{asset('img/h_exit@2x.png')}}" width="29">

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    @endguest
                </div>
                </div>
            </div>
            <div class="header-down-wrapper">

                <div class="inner-wrapper-down">

                    <a class="navbar-toggler">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>

                    <ul class="custom-navigation">
                        <li class="menu-item">
                            <a class="menu-item-link with_arrow" href="https://kisskiss.show/livecam">Livecams</a>
                            <div class="custom-item-elemetns hide">
                                <ul>
                                    <li><a href="https://kisskiss.show/livecam/filter/anal">Anal</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/blondes">Blondes</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/blowjob">Blowjob</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/brunettes">Brunettes</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/couples">Couples</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/fingering">Fingering</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/lesbian">Lesbian</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/redhead">Redhead</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/sex_toys">Sex toys</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/small_tits">Small Tits</a></li>
                                    <li><a href="https://kisskiss.show/livecam/filter/threesome">Threesome</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="menu-item">
                            <a class="menu-item-link" href="https://kisskiss.show/dating">Sexdating</a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-item-link with_arrow" href="https://kisskiss.show/videos">Videos</a>
                            <div class="custom-item-elemetns hide">
                                <ul>
                                    <li><a href="https://kisskiss.show/videos?filters=amateur">Amateur</a></li>
                                    <li><a href="https://kisskiss.show/videos?filters=big_tits">Big tits</a></li>
                                    <li><a href="https://kisskiss.show/videos?filters=fingering">Fingering</a></li>
                                    <li><a href="https://kisskiss.show/videos?filters=webcam">Webcam</a></li>
                                </ul>
                            </div>
                        </li>

                        <li class="menu-item">
                            <a class="menu-item-link" href="https://kisskiss.show/articles">Articles</a>
                        </li>

                        <li class="menu-item">
                            <a class="menu-item-link with_arrow" href="https://kisskiss.show/instaporn">Instaporn</a>
                            <div class="custom-item-elemetns hide">
                                <ul>
                                    @foreach(\App\Models\Category::withoutParents() as $category)
                                        <li><a href="{{route('instaporn-category',$category->url_noslug())}}">{{$category->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>



        {{--</nav>--}}
    </div>
</header>
<main>
    @yield('content')
</main>
<footer>
    <div class="container-fluid px-0">
        <div class="main_footer">
            <nav class="quick_links">
                <a class="mf_link" href="https://kisskiss.show/livecam/wiki">Wiki &amp; FAQ</a>
                <a class="mf_link" href="/livecam/reg/model">Model Registration</a>
                <a class="mf_link" href="/">Home</a>
                <a class="mf_link">Online Support</a>
                <a class="mf_link" href="/sitemap">Sitemap</a>
            </nav>
            <div class="langs">
                <a class="mf_lang actual ng-star-inserted">En</a>
                <a class="mf_lang ng-star-inserted">De</a>
                <a class="mf_lang ng-star-inserted">Ru</a>
                <a class="mf_lang ng-star-inserted">Fr</a>
                <a class="mf_lang ng-star-inserted">Sp</a>
                <a class="mf_lang ng-star-inserted">It</a>
            </div>
            <div class="mf_soc">
                <a class="facebook"></a>
                <a class="instagram"></a>
                <a class="twitter"></a>
            </div>
        </div>

        <div class="bottom_footer px-2">
            <div class="bottom_footer_left">
                <p>The site contains sexually explicit material. Enter ONLY if you are at least 18 years old and agree to our cookie rules. <a href="http://www.rtalabel.org/" target="_blank">RTA</a>  <a href="http://www.asacp.org/ASACP.php" target="_blank">ASACP</a>  <a href="http://www.fosi.org/icra/" target="_blank">ICRA</a></p>
            </div>
            <div class="bottom_footer_right">
                <div class="bfr_links">
                    <a href="/livecam/terms_conditions">Terms &amp; Conditions</a>
                    <a href="/livecam/antispam_policy">Anti-Spam Policy</a>
                    <a href="/livecam/refund_policy">Refund Policy</a>
                    <a href="/livecam/privacy_policy">Privacy Policy</a>
                    <a href="/livecam/ownership_statement">Ownership statement</a>
                </div>
                <p class="copyring">© 2018 Live Communications LTD - All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
