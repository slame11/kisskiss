@extends('layouts.client')

@section('title', __('general.category'))
@section('content-title', __('general.category'))

@section('content')
    <div class="col-md-3">
        <div class="card mb-3 shadow-sm">
            <a href="{{route('image',$image)}}">
                <img width="100%" src="/{{ $image->src() }}" alt="{{ $image->title }}">
            </a>
            <div class="card-body">
                <a href="{{route('image',$image)}}">
                    <p class="card-text">{{ $image->title }}</p>
                </a>
                <div class="d-flex justify-content-between align-items-center">
                    @foreach($image->categories as $category)
                        <a href="{{route('image-category',$category)}}"><button type="button" class="btn btn-sm btn-outline-secondary" >{{ $category->title }}</button></a>
                    @endforeach
                    <small class="text-muted"><a href="/{{ $image->src() }}" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a></small>
                </div>
            </div>
        </div>
    </div>
@endsection
