@extends('admin.layouts.admin-lte')

@section('title', __('general.dashboard'))
@section('content-title', __('general.dashboard'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li class="active"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
        </div>
    </div>
@endsection
