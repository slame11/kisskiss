
<form method="post" action="{{route('google-search.search')}}">
    {{ csrf_field() }}

    <div class="form-group form-row">
        <label for="search-mime" class="col">Image type</label>
        <div class="col">
            <select class="form-control" name="search-mime">
                <option value="*" @if(isset($data['search-mime']) && $data['search-mime'] == "*") selected @endif>Any</option>
                <option value="gif" @if(isset($data['search-mime']) && $data['search-mime'] == "gif") selected @endif>GIF</option>
                <option value="jpg" @if(isset($data['search-mime']) && $data['search-mime'] == "jpg") selected @endif>JPEG</option>
                <option value="png" @if(isset($data['search-mime']) && $data['search-mime'] == "png") selected @endif>PNG</option>
            </select>
        </div>
        <label for="search-count" class="col">Images per page</label>
        <div class="col">
            <input type="number" value="10" class="form-control" name="search-count" placeholder="Results count" min="1" max="10">
        </div>
        <label for="search-count" class="col">Search query</label>
        <div class="col">
            <input type="text" class="form-control" name="search-query" placeholder="Search query" value="{{$data['search-query']??null}}">
        </div>
        <label for="search-mime" class="col">Google Image type</label>
        <div class="col">
            <select class="form-control" name="imgType">
                <option value=""></option>
                <option value="clipart" @if(isset($data['imgType']) && $data['imgType'] == "clipart") selected @endif> clipart </option>
                <option value="face" @if(isset($data['imgType']) && $data['imgType'] == "face") selected @endif> face </option>
                <option value="lineart" @if(isset($data['imgType']) && $data['imgType'] == "lineart") selected @endif> lineart </option>
                <option value="news" @if(isset($data['imgType']) && $data['imgType'] == "news") selected @endif> news </option>
                <option value="photo" @if(isset($data['imgType']) && $data['imgType'] == "photo") selected @endif> photo </option>
            </select>
        </div>
        <label for="search-mime" class="col">Google Image syze</label>
        <div class="col">
            <select class="form-control" name="imgSize">
                <option value=""></option>
                <option value="icon" @if(isset($data['imgSize']) && $data['imgSize'] == "icon") selected @endif> icon </option>
                <option value="small" @if(isset($data['imgSize']) && $data['imgSize'] == "small") selected @endif> small </option>
                <option value="medium" @if(isset($data['imgSize']) && $data['imgSize'] == "medium") selected @endif> medium </option>
                <option value="large" @if(isset($data['imgSize']) && $data['imgSize'] == "large") selected @endif> large </option>
                <option value="xlarge" @if(isset($data['imgSize']) && $data['imgSize'] == "xlarge") selected @endif> xlarge </option>
                <option value="xxlarge" @if(isset($data['imgSize']) && $data['imgSize'] == "xxlarge") selected @endif> xxlarge </option>
                <option value="huge" @if(isset($data['imgSize']) && $data['imgSize'] == "huge") selected @endif> huge </option>
            </select>
        </div>

    </div>
    <div class="form-group form-row">
        <button type="submit" class="btn btn-success py-3">Search</button>

        @if(isset($pagination) && count($pagination) > 3)
        <ul class="pagination pagination-sm no-margin pull-right">
            @forelse($pagination as $page)
                <li><button class="btn btn-adn {{$page['class']??null}}" href="#" type="sumbit" value="{{$page['val']??null}}" name="search-start">{{$page['name']??null}}</button></li>
            @endforeach
        </ul>
        @endif
    </div>

</form>
