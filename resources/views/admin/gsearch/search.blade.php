@extends('admin.layouts.admin-lte')

@section('title', __('general.images'))
@section('content-title', 'Google Search Import')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('google-search.index') }}"><i class="fa fa-building"></i> {{ __('general.search') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('general.new') }}</li>
    </ol>
@endsection

@section('content')


    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header ">
                    <h3>Search: {!! $data['search-query'] !!}</h3>
                </div>
                <div class="box-body">
                    @include('admin.gsearch.partials.form')

                </div>
            </div>

            <form method="post" action="{{route('google-search.search')}}">
            <div class="box box-primary">
                <div class="box-header ">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="category[]">Select categories</label>
                            <select class="form-control select2 select2-hidden-accessible" multiple="" name="category[]" data-placeholder="Select categories" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach(\App\Models\Category::withoutParents() as $category)
                                    <option value="{{$category->id}}" {!! in_array($category->id,$category_selected ) ? 'selected' : '' !!}>{{$category->title}}</option>
                                    @if(!empty($category->children) && count($category->children)>0)
                                        <optgroup label="{{$category->title}}">
                                            @foreach($category->children as $child)
                                                <option value="{{$child->id}}" {!! in_array($child->id,$category_selected ) ? 'selected' : '' !!}>{{$child->title}}</option>
                                            @endforeach
                                        </optgroup>
                                        <option disabled>_________</option>
                                    @endif
                                @endforeach
                            </select>

                        </div>

                        <div class="col-sm-6">
                            <div class="pull-right">
                                <input class="btn btn-success" type="submit" value="Save">
                            </div>
                        </div>

                    </div>

                    <script>
                        document.addEventListener('DOMContentLoaded', function(){
                            $('.select2').select2();
                        });
                    </script>
                </div>
                <div class="box-body">
                    {{csrf_field()}}
                    <input hidden type="text" name="search-start" value="{{$parameters['start']??null}}" >
                    <input hidden type="text" name="search-count" value="{{$data['search-count']??10}}"  >
                    <input hidden type="text" name="search-query" value="{{$data['search-query']??null}}" >
                    <input hidden type="text" name="search-mime" value="{{$data['search-mime']??null}}" >
                    <input hidden type="text" name="imgType" value="{{$data['imgType']??null}}" >
                    <input hidden type="text" name="imgSize" value="{{$data['imgSize']??null}}" >
                    {{--<input hidden type="text" name="category" value="{{ $data['category']??'PIC' }}">--}}
                    <input hidden type="text" name="created_by" value="{{ Auth::id() }}">

                    {{--@foreach($results as $result)--}}
                        {{--@if(isset($result->link))--}}
                            {{--<div class="attachment-block clearfix col-sm-3">--}}
                                {{--<img class="attachment-img" src="{{ $result->link }}" alt="Attachment Image">--}}

                                {{--<div class="attachment-pushed">--}}
                                    {{--<h4 class="attachment-heading"><a href="{{ $result->image->contextLink }}">{!! htmlspecialchars_decode($result->htmlTitle) !!}</a></h4>--}}

                                    {{--<div class="attachment-text">--}}
                                        {{--<button type="button"--}}
                                                {{--onclick="$(this).parent('div').parent('div').parent('div').remove();"--}}
                                                {{--class="btn-xs remove_item btn btn-danger pull-right"><i--}}
                                                {{--class="fa fa-trash"></i></button>--}}
                                    {{--</div>--}}
                                    {{--<!-- /.attachment-text -->--}}
                                {{--</div>--}}
                                {{--<!-- /.attachment-pushed -->--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}


                        <div class="row-no-gutters py-3">
                            <ul class="grid">
                            {{--<ul class="mailbox-attachments clearfix">--}}
                                @foreach($results as $result)
                                    @if(isset($result->link))

                                        {{--<li>--}}
                                        <li class="grid-item">
                                            <div class="grid-item-wrapper">
                                                <span class="mailbox-attachment-icon has-img">
                                                    <a href="{{ $result->link }}" data-fancybox>
                                                        <img src="{{ $result->link }}" alt="Attachment">
                                                    </a>
                                                </span>

                                                <div class="mailbox-attachment-info">
                                                    <a href="{{ $result->image->contextLink }}"
                                                       class="mailbox-attachment-name"> {!! htmlspecialchars_decode($result->htmlTitle) !!}</a>
                                                    <span class="mailbox-attachment-size">
                                                            .....
                                                            {{--<button type="button"--}}
                                                                {{--onclick="$(this).parent('span').parent('div').parent('div').parent('li').remove();"--}}
                                                                {{--class="btn-xs remove_item btn btn-danger pull-right">--}}
                                                                {{--<i class="fa fa-trash"></i>--}}
                                                            {{--</button>--}}
                                                        <button type="button"
                                                                class="btn-xs remove_item btn btn-danger pull-right">
                                                                <i class="fa fa-trash"></i>
                                                            </button>

                                                        </span>
                                                </div>

                                                <input hidden type="text" name="images[{{$i+=1}}][name]" value="{{ $result->title }}">
                                                <input hidden type="text" name="images[{{$i}}][src]" value="{{ $result->link }}">
                                                <input hidden type="text" name="created_by" value="{{ Auth::id() }}">
                                            </div>
                                        </li>
                                    @endif
                                @endforeach

                            </ul>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
<script>
    document.addEventListener('DOMContentLoaded', function(){
        $('.select2').select2();
        // $('.grid').packery();
    });
</script>
@endsection
