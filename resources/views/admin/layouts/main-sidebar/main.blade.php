<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

    <!-- Sidebar Menu -->
        @php
            list($currentRouteType) = explode('.', Route::currentRouteName())
        @endphp
        @section('sidebar-menu')
            <ul class="sidebar-menu" data-widget="tree">
                @can('admin')
                    <li @if (in_array($currentRouteType, ['dashboard']) ) class="active" @endif>
                        <a href="{{ route('dashboard') }}" class="text-gray">
                            <i class="fa fa-dashboard"></i>
                            <span>{{ __('general.dashboard') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['category']) ) class="active" @endif>
                        <a href="{{ route('category.index') }}" class="text-gray">
                            <i class="fa fa-list"></i>
                            <span>{{ __('general.categories') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['images']) ) class="active" @endif>
                        <a href="{{ route('images.index') }}" class="text-gray">
                            <i class="fa fa-list"></i>
                            <span>{{ __('general.images') }}</span>
                        </a>
                    </li>
                    <li @if (in_array($currentRouteType, ['google-search']) ) class="active" @endif>
                        <a href="{{ route('google-search.index') }}" class="text-gray">
                            <i class="fa fa-google"></i>
                            <span>Google Search Import</span>
                        </a>
                    </li>

                    <li class="treeview @if (in_array($currentRouteType, ['users', 'countries', 'cities',]) ) active @endif">
                        <a href="#" class="text-gray">
                            <i class="fa fa-gear"></i>
                            <span>{{ __('general.settings') }}</span>
                        </a>
                        <ul class="treeview-menu">
                            <li @if (in_array($currentRouteType, ['users']) ) class="active" @endif>
                                <a href="{{ route('users.index') }}" class="text-gray">
                                    <i class="fa fa-users"></i>
                                    <span>{{ __('general.users') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['countries']) ) class="active" @endif>
                                <a href="{{ route('countries.index') }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.countries') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['cities']) ) class="active" @endif>
                                <a href="{{ route('cities.index') }}" class="text-gray">
                                    <i class="fa fa-globe"></i>
                                    <span>{{ __('general.cities') }}</span>
                                </a>
                            </li>
                            <li @if (in_array($currentRouteType, ['generalSettings']) ) class="active" @endif>
                                <a href="{{ route('generalSettings.index') }}" class="text-gray">
                                    <i class="fa fa-tasks"></i>
                                    <span>{{ __('general.general_settings') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan
            </ul>
    @show
    <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
