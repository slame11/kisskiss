<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
            {!! Form::label('country_id', __('general.country')) !!}
            {!! Form::select(
            'country_id',
             $countries,
             $city->country_id ?? null,
             ['class' => 'form-control']
             ) !!}
            {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('map_point') ? 'has-error' : ''}}">
            {!! Form::label('map_point', __('general.map_point')) !!}
            {!! Form::text('map_point',$city->map_beach_point ?? '0 0',['class' => 'form-control','placeholder' => __('general.map_point'),'size' => 255]) !!}
            {!! $errors->first('map_point', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group  {{ $errors->has('zip_code') ? 'has-error' : ''}}">
            {!! Form::label('zip_code', __('general.zip_code')) !!}
            {!! Form::text('zip_code',$city->zip_code ,['class' => 'form-control', 'min'=>0,'max'=>18]) !!}
            {!! $errors->first('zip_code', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
</div>

@include('admin.city.partials.translations')