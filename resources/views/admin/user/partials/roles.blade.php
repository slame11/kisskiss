@foreach($roles as $role)
    <option value="{{ isset($role->id)?$role->id:null }}"
        @isset($user->roles)
            @foreach($user->roles as $user_role)
                @if($role->id == $user_role->id)
                    selected="selected"
                @endif
            @endforeach
        @endisset
    >
        {{ isset($role->name)?$role->name:null }}
    </option>

@endforeach
