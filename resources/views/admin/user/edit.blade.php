@extends('admin.layouts.admin-lte')

@section('title', __('general.users'))
@section('content-title', __('general.users'))


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('users.index') }}"><i class="fa fa-star"></i> {{ __('general.users') }}</a></li>
        <li class="active"><i class="fa fa-star"></i> {{ __('general.edit') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.edit') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success', 'form' => 'user_create_form']) !!}

                        <a href="{{ route('users.index') }}" class="btn btn-default"><i class="fa fa fa-share"></i></a>

                    </div>
                </div>
                {!! Form::model($user, ['route' => ['users.update', $user],'method' => 'put','files' => true, 'id' => 'user_create_form']) !!}
                <div class="box-body">
                    @include('admin.user.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {

            function randString(id){
                var dataSet = $(id).attr('data-character-set').split(',');
                var possible = '';
                if($.inArray('a-z', dataSet) >= 0){
                    possible += 'abcdefghijklmnopqrstuvwxyz';
                }
                if($.inArray('A-Z', dataSet) >= 0){
                    possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                }
                if($.inArray('0-9', dataSet) >= 0){
                    possible += '0123456789';
                }
                if($.inArray('#', dataSet) >= 0){
                    possible += '![]{}()%&*$#^<>~@|';
                }
                var text = '';
                for(var i=0; i < $(id).attr('data-size'); i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            }

            // Create a new password
            $(".getNewPass").click(function(){
                var field = $(this).closest('div').find('input[rel="gp"]');
                var password = randString(field);
                field.val(password).prop("type", "text");
                $('input[rel="gp_confirm"]').val(password).prop("type", "text");
            });

            // Auto Select Pass On Focus
            $('input[rel="gp"]').on("click", function () {
                $(this).select();
            });


        });
    </script>
@endsection
