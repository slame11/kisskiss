@extends('admin.layouts.admin-lte')

@section('title', __('general.users_list'))
@section('content-title', __('general.users_list'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-thumbs-up"></i>{{ __('general.users') }}</li>
    </ol>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('general.users') }}</h3>
        <div class="pull-right">
            <a href="{{ route('users.create') }}" class="btn btn-sm btn-success add_new_user">
                {{ __('general.new') }}
            </a>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-hover table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>{{ __('general.user') }}</th>
                <th>{{ __('general.email') }}</th>
                <th>{{ __('general.users_roles') }}</th>
                <th>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->roles()->pluck('name')->implode(', ') }}</td>
                    <td class="td-action">
                        <a href="{{ route('users.edit', $user) }}" class="btn btn-success btn-sm pull-left">
                            <i class="fa fa-pencil"></i>
                        </a>

                        <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('users.destroy', $user) }}" method="post">
                            <input type="hidden" name="_method" value="delete" />
                            {{csrf_field()}}
                            <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td><h2>{{ __('general.empty_data') }}</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <nav aria-label="page navigation">
            <ul class="pagination">
                {{$users->links()}}
            </ul>
        </nav>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#users_list').DataTable({
                "aoColumnDefs": [
                    {"bSortable": false, "aTargets": [3, 4]}
                ]
            });
        });
    </script>
@endsection
