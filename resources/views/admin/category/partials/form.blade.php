<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            {!! Form::label('title', __('general.title')) !!}
            {!! Form::text('title', $category->title ?? '',['class' => 'form-control','placeholder' => __('general.title'),'size' => 255]) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    @if(in_array($category->id,\App\Models\Category::GLOBAL_CATEGORIES))
        {!! Form::hidden('parent_id', $category->parent_id) !!}
    @else
        <div class="col-sm-6">

            <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
                {!! Form::label('parent_id', __('general.category_parent')) !!}
                {!! Form::select(
                'parent_id',
                 $categories_global,
                 $category->parent_id ?? null,
                 ['class' => 'form-control']
                 ) !!}
                {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
            </div>

        </div>
    @endif

    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('meta') ? 'has-error' : ''}}">
            {!! Form::label('meta', __('general.category_meta')) !!}
            {!! Form::textarea('meta',$category->meta ?? '',['class' => 'form-control','placeholder' => __('general.category_meta')]) !!}
            {!! $errors->first('meta', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            {!! Form::label('description', __('general.description')) !!}
            {!! Form::textarea('description',$category->description ?? '',['class' => 'form-control','placeholder' => __('general.description')]) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('text') ? 'has-error' : ''}}">
            {!! Form::label('text', __('general.text')) !!}
            {!! Form::textarea('text',$category->text ?? '',['class' => 'form-control','placeholder' => __('general.text')]) !!}
            {!! $errors->first('text', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
</div>

{{--@include('admin.category.partials.translations')--}}
