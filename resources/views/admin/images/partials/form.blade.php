<style>
    .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
    .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
    .percent { position:absolute; display:inline-block; top:0px; left:48%; color: #7F98B2;}
    .uploaded { font-weight: bold; }
</style>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            {!! Form::label('images[]', __('general.images')) !!}
            {!! Form::file('images[]', ['accept'=>'image/*','class' => 'form-control','placeholder' => __('general.title'), 'multiple'=>'multiple']) !!}
            {!! $errors->first('images[]', '<p class="help-block">:message</p>') !!}

            <div class="progress" style="display:none; margin-top: 10px;">
                <div class="bar"></div >
                <div class="percent">0%</div >
            </div>
            <ul class="images_list" style="list-style-type:none;"></ul>
        </div>
    </div>
</div>
