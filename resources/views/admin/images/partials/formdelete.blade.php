{!! Form::open(['route' => ['images.destroy',$image],'method' => 'delete','hidden']) !!}
{!! Form::close() !!}

<form action="/admin/images/{{$image->id }}" method="POST">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
</form>
