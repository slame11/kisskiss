<?php

use Illuminate\Database\Seeder;

class ImagesThumbnailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imgs = \Illuminate\Support\Facades\DB::select( \Illuminate\Support\Facades\DB::raw("SELECT `id` FROM `image_d_b_s` WHERE `path`=`thumbnail`") );
        foreach ($imgs as $img)
        {
            $imageDB = \App\Models\ImageDB::find($img->id);
            $imageDB->thumbnail =  \App\Models\ImageTool::resizeFromStorage(storage_path('app/'.$imageDB->path),300,0);
            $imageDB->save();
        }
    }
}
