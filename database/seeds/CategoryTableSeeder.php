<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title' => 'GIF',
            ],
            [
                'title' => 'PIC',
            ],
        ];

        foreach ($categories as $data) {
            \App\Models\Category::create($data);
        }
    }
}
