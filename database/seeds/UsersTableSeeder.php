php artisan migrate:refresh --seed<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Administrator',
                'email' => 'admin@admin.com',
                'email_verified_at' => '2019-01-01 00:00:00',
                'password' => '111111',
                'roles'      => [
                    'Admin',
                ],
            ],
        ];

        foreach ($users as $data) {
            $user = \App\Models\User::create(
                [
                    'email'      => $data['email'],
                    'email_verified_at' => $data['email_verified_at'],
                    'name'       => $data['name'],
                    'password'   => $data['password'],
                ]
            );
            foreach ($data['roles'] as $r) {
                $role = \Spatie\Permission\Models\Role::firstOrCreate(['name' => $r]);
                $user->assignRole($role);
            }
        }
    }
}
